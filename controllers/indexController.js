var orderModel = require('../models/orderModel.js');
var customerModel = require('../models/customerModel.js');
var productModel = require('../models/productModel.js');
var ingredientModel = require('../models/ingredientModel.js');

/**
 * indexController.js
 *
 * @description :: Server-side logic for managing index.
 */
module.exports = {

  /**
   * indexController.show()
   */
  show: function(req, res) {
    var data = {};
    orderModel.count(function(err, orders) {
      if (err) {
        return res.json(500, {
          message: 'Error getting orders.'
        });
      }
      data.orders = orders;
      customerModel.count(function(err, customers) {
        if (err) {
          return res.json(500, {
            message: 'Error getting customers.'
          });
        }
        data.customers = customers;
        productModel.count(function(err, products) {
          if (err) {
            return res.json(500, {
              message: 'Error getting products.'
            });
          }
          data.products = products;
          ingredientModel.count(function(err, ingredients) {
            if (err) {
              return res.json(500, {
                message: 'Error getting ingredients.'
              });
            }
            data.ingredients = ingredients;
            res.json(data);
          });
        });
      });
    });
  }

};
