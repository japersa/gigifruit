var unitModel = require('../models/unitModel.js');

/**
 * unitController.js
 *
 * @description :: Server-side logic for managing units
 */
module.exports = {

    /**
     * unitController.list()
     */
    list: function(req, res) {
        unitModel.find(function(err, units) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting units.'
                });
            }
            return res.json(units);
        });
    },
    /**
     * unitController.listStatus()
     */
    listStatus: function(req, res) {
        var status = req.params.status;

        unitModel.find({
            status: status
        }, function(err, units) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting units.'
                });
            }
            return res.json(units);
        });
    },
    /**
     * unitController.list()
     */
    show: function(req, res) {
        unitModel.findOne({
            code: req.params.code
        }, function(err, unit) {
            console.log(unit);
            if (err) {
                return res.status(500).json({
                    message: 'Error getting unit.'
                });
            }
            return res.json(unit);
        });
    },
    /**
     * unitController.create()
     */
    create: function(req, res) {

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('abbreviation', 'abbreviation is required').notEmpty();


        var errors = req.validationErrors();
        if (!errors) {

            unitModel.findOne({
                'name': req.body.name
            }, function(err, rows) {
                if (err) {
                    return res.status(404).json({
                        message: 'Error searching unit',
                        error: err
                    });
                }
                if (rows) {
                    return res.status(500).json({
                        message: 'The unit already exists'
                    });
                } else {
                  
                    var unit = new unitModel({
                        code: new Date().getTime(),
                        name: req.body.name,
                        abbreviation: req.body.abbreviation,
                        description: req.body.description
                    });

                    unit.save(function(err, unit) {
                        if (err) {
                            return res.json(500, {
                                message: 'Error saving unit',
                                error: err
                            });
                        }
                        return res.json({
                            message: 'saved',
                            _id: unit._id
                        });
                    });
                }
            });
        } else {
            return res.status(500).json({
                title: 'Validation unit',
                message: 'Failure to register due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * unitController.update()
     */
    update: function(req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('abbreviation', 'abbreviation is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            unitModel.findOne({
                code: code
            }, function(err, unit) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting unit',
                        error: err
                    });
                }
                if (!unit) {
                    return res.status(404).json({
                        message: 'No such unit'
                    });
                }

                var result = unit;

                unitModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function(err, unit) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting unit',
                            error: err
                        });
                    }

                    if (unit) {
                        if (unit.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                              result.abbreviation = req.body.abbreviation ? req.body.abbreviation.toLowerCase() : result.abbreviation;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.updated_at = new Date();

                            result.save(function(err, unit) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving unit.'
                                    });
                                }
                                if (!unit) {
                                    return res.status(404).json({
                                        message: 'No such unit'
                                    });
                                }
                                return res.json(unit);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'unit type already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.abbreviation = req.body.abbreviation ? req.body.abbreviation.toLowerCase() : result.abbreviation;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.updated_at = new Date();

                        result.save(function(err, unit) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving unit.'
                                });
                            }
                            if (!identificationType) {
                                return res.status(404).json({
                                    message: 'No such unit'
                                });
                            }
                            return res.json(unit);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update unit',
                message: 'Failure to update unit due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * unitController.status()
     */
    status: function(req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            unitModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function(err, unit) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting unit.'
                    });
                }

                if (!unit) {
                    return res.status(404).json({
                        message: 'No such unit'
                    });
                }

                return res.json(unit);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status unit',
                message: 'Failure to status unit due to some validation error',
                errors: errors
            });
        }
    }
};
