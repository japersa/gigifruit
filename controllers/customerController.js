var customerModel = require('../models/customerModel.js');
var identificationTypeModel = require('../models/identificationTypeModel.js');
/**
 * customerController.js
 *
 * @description :: Server-side logic for managing customers.
 */
module.exports = {

  /**
   * customerController.list()
   */
  list: function(req, res) {
    customerModel.find(function(err, customers) {
        if (err) {
          return res.status(500).json({
            message: 'Error getting customers.'
          });
        }
        return res.json(customers);
    });
  },

  /**
   * customerController.listStatus()
   */
  listStatus: function(req, res) {
    var status = req.params.status;

    customerModel.find({
      status: status
    }, function(err, customers) {

        if (err) {
          return res.status(500).json({
            message: 'Error getting customers.'
          });
        }
        return res.json(customers);

    });
  },

  /**
   * customerController.show()
   */
  show: function(req, res) {
    var code = req.params.code;

    customerModel.findOne({
      code: code
    }, function(err, customer) {
          if (err) {
          return res.status(500).json({
            message: 'Error getting customer.'
          });
        }
        if (!customer) {
          return res.status(404).json({
            message: 'No such customer'
          });
        }
        return res.json(customer);
    });
  },
  /**
   * customerController.create()
   */
  create: function(req, res) {

    req.checkBody('name', 'name is required').notEmpty();
    req.checkBody('identification_type', 'identification_type is required').notEmpty();
    req.checkBody('id_document', 'id_document is required').notEmpty();
    req.checkBody('email', 'email is required').isEmail();
    req.checkBody('phone', 'phone is required').notEmpty();

    var errors = req.validationErrors();
    if (!errors) {
      customerModel.findOne({
        id_document: req.body.id_document
      }, function(err, customer) {
        if (err) {
          return res.status(500).json({
            message: 'Error getting customer',
            error: err
          });
        }
        if (customer) {
          return res.status(500).json({
            message: 'Customer already exist'
          });
        }

        var customer = new customerModel({
          code: new Date().getTime(),
          name: req.body.name.toLowerCase(),
          identification_type: req.body.identification_type,
          id_document: req.body.id_document,
          address: req.body.address ? req.body.address.toLowerCase() : "",
          email: req.body.email.toLowerCase(),
          phone: req.body.phone,
          mobile_phone: req.body.mobile_phone
        });

        customer.save(function(err, customer) {
          if (err) {
            return res.status(500).json({
              message: 'Error saving customer',
              error: err
            });
          }
          return res.json({
            message: 'saved',
            _id: customer._id
          });
        });
      });
    } else {
      return res.status(500).json({
        title: 'Validation create customer',
        message: 'Failure to create customer due to some validation error',
        errors: errors
      });
    }
  },

  /**
   * customerController.update()
   */
  update: function(req, res) {
    var code = req.params.code;

    req.checkBody('name', 'name is required').notEmpty();
    req.checkBody('identification_type', 'identification_type is required').notEmpty();
    req.checkBody('id_document', 'id_document is required').notEmpty();
    req.checkBody('email', 'email is required').isEmail();
    req.checkBody('phone', 'phone is required').notEmpty();

    var errors = req.validationErrors();
    if (!errors) {
      customerModel.findOne({
        code: code
      }, function(err, customer) {
        if (err) {
          return res.status(500).json({
            message: 'Error getting customer',
            error: err
          });
        }
        if (!customer) {
          return res.status(404).json({
            message: 'No such customer'
          });
        }

        var result = customer;

        customerModel.findOne({
          id_document: req.body.id_document
        }, function(err, customer) {
          if (err) {
            return res.status(500).json({
              message: 'Error getting customer',
              error: err
            });
          }

          if (customer) {
            if (customer.code == code) {
              result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
              result.identification_type = req.body.identification_type ? req.body.identification_type : result.identification_type;
              result.id_document = req.body.id_document ? req.body.id_document : result.id_document;
              result.address = req.body.address ? req.body.address.toLowerCase() : "",
              result.email = req.body.email ? req.body.email.toLowerCase() : result.email;
              result.phone = req.body.phone ? req.body.phone : result.phone;
              result.mobile_phone = req.body.mobile_phone ? req.body.mobile_phone : result.mobile_phone;
              result.updated_at = new Date();

              result.save(function(err, customer) {
                if (err) {
                  return res.status(500).json({
                    message: 'Error saving customer.'
                  });
                }
                if (!customer) {
                  return res.status(404).json({
                    message: 'No such customer'
                  });
                }
                return res.json(customer);
              });
            } else {
              return res.status(500).json({
                message: 'Customer already exist'
              });
            }
          } else {
            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
            result.identification_type = req.body.identification_type ? req.body.identification_type : result.identification_type;
            result.id_document = req.body.id_document ? req.body.id_document : result.id_document;
            result.address = req.body.address ? req.body.address.toLowerCase() : "",
            result.email = req.body.email ? req.body.email : result.email;
            result.phone = req.body.phone ? req.body.phone : result.phone;
            result.mobile_phone = req.body.mobile_phone ? req.body.mobile_phone : result.mobile_phone;
            result.updated_at = new Date();

            result.save(function(err, customer) {
              if (err) {
                return res.status(500).json({
                  message: 'Error saving customer.'
                });
              }
              if (!customer) {
                return res.status(404).json({
                  message: 'No such customer'
                });
              }
              return res.json(customer);
            });
          }
        });
      });
    } else {
      return res.status(500).json({
        title: 'Validation update customer',
        message: 'Failure to update customer due to some validation error',
        errors: errors
      });
    }
  },

  /**
   * customerController.status()
   */
  status: function(req, res) {

    var code = req.params.code;

    req.checkBody('status', 'status is required').notEmpty();

    var errors = req.validationErrors();
    if (!errors) {
      customerModel.findOneAndUpdate({
        code: code
      }, {
        status: req.body.status
      }, function(err, customer) {
        if (err) {
          return res.json(500, {
            message: 'Error getting customer.'
          });
        }

        if (!customer) {
          return res.status(404).json({
            message: 'No such customer'
          });
        }

        return res.json(customer);
      });
    } else {
      return res.status(500).json({
        title: 'Validation status customer',
        message: 'Failure to status customer due to some validation error',
        errors: errors
      });
    }
  }
};
