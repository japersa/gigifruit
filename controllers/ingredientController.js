var ingredientModel = require('../models/ingredientModel.js');

/**
 * ingredientController.js
 *
 * @description :: Server-side logic for managing ingredients
 */

module.exports = {

    /**
     * ingredientController.list()
     */
    list: function (req, res) {
        ingredientModel.find(function (err, ingredients) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting ingredients.'
                });
            }
            return res.json(ingredients);
        });
    },

    /**
     * ingredientController.listStatus()
     */
    listStatus: function (req, res) {
        var status = req.params.status;

        ingredientModel.find({
            status: status
        }, function (err, ingredients) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting ingredients.'
                });
            }
            return res.json(ingredients);
        });
    },

    /**
     * ingredientController.show()
     */
    show: function (req, res) {
        var code = req.params.code;

        ingredientModel.findOne({
            code: code
        }, function (err, ingredient) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting ingredient.'
                });
            }
            if (!ingredient) {
                return res.status(404).json({
                    message: 'No such ingredient'
                });
            }
            return res.json(ingredient);
        });
    },
    /**
     * ingredientController.create()
     */
    create: function (req, res) {

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('quantity', 'quantity is required').notEmpty();
        req.checkBody('unit', 'unit is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            ingredientModel.findOne({
                name: req.body.name.toLowerCase()
            }, function (err, ingredient) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting ingredient',
                        error: err
                    });
                }
                if (ingredient) {
                    return res.status(500).json({
                        message: 'ingredient already exist'
                    });
                }
                var ingredient = new ingredientModel({
                    unit: req.body.unit,
                    code: new Date().getTime(),
                    name: req.body.name.toLowerCase(),
                    description: req.body.description ? req.body.description.toLowerCase() : "",
                    quantity: req.body.quantity
                });

                ingredient.save(function (err, ingredient) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error saving ingredient',
                            error: err
                        });
                    }
                    return res.json({
                        message: 'saved',
                        _id: ingredient._id
                    });
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation create ingredient',
                message: 'Failure to create ingredient due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * ingredientController.update()
     */
    update: function (req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('unit', 'unit is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            ingredientModel.findOne({
                code: code
            }, function (err, ingredient) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting ingredient',
                        error: err
                    });
                }
                if (!ingredient) {
                    return res.status(404).json({
                        message: 'No such ingredient'
                    });
                }

                var result = ingredient;

                ingredientModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function (err, ingredient) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting ingredient',
                            error: err
                        });
                    }

                    if (ingredient) {
                        if (ingredient.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.unit = req.body.unit ? req.body.unit : result.unit;
                            result.updated_at = new Date();

                            result.save(function (err, ingredient) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving ingredient.'
                                    });
                                }
                                if (!ingredient) {
                                    return res.status(404).json({
                                        message: 'No such ingredient'
                                    });
                                }
                                return res.json(ingredient);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'Ingredient already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.unit = req.body.category ? req.body.category : result.unit;
                        result.updated_at = new Date();

                        result.save(function (err, ingredient) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving ingredient.'
                                });
                            }
                            if (!ingredient) {
                                return res.status(404).json({
                                    message: 'No such ingredient'
                                });
                            }
                            return res.json(ingredient);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update ingredient',
                message: 'Failure to update ingredient due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * ingredientController.status()
     */
    status: function (req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            ingredientModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function (err, ingredient) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting ingredient.'
                    });
                }

                if (!ingredient) {
                    return res.status(404).json({
                        message: 'No such ingredient'
                    });
                }

                return res.json(ingredient);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status ingredient',
                message: 'Failure to status ingredient due to some validation error',
                errors: errors
            });
        }
    }
};
