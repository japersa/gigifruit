var orderModel = require('../models/orderModel.js');
var ingredientModel = require('../models/ingredientModel.js');

/**
 * orderController.js
 *
 * @description :: Server-side logic for managing orders
 */
module.exports = {

    /**
     * orderController.list()
     */
    list: function(req, res) {
        orderModel.find(function(err, orders) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting orders.'
                });
            }
            return res.json(orders);
        });
    },


    /**
     * orderController.generateConsecutive()
     */
    generateConsecutive: function(req, res) {
        // add zeros to left
        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

        orderModel.count({}, function(err, count) {
            if (err) {
                return res.json(500, {
                    message: 'Error getting orders.'
                });
            }

            res.json({
                consecutive: zeroPad(count + 1, 6),
                currentDate: new Date()
            });

        });
    },
    create: function(req, res){
      console.log(req.body);
    }
};
