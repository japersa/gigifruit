var categoryModel = require('../models/categoryModel.js');

/**
 * categoryController.js
 *
 * @description :: Server-side logic for managing categories.
 */
module.exports = {

    /**
     * categoryController.list()
     */
    list: function(req, res) {
        categoryModel.find(function(err, categories) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting categories.'
                });
            }
            return res.json(categories);
        });
    },
    /**
     * categoryModel.listStatus()
     */
    listStatus: function(req, res) {
        var status = req.params.status;

        categoryModel.find({
            status: status
        }, function(err, category) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting categories.'
                });
            }
            return res.json(category);
        });
    },
    /**
     * categoryController.list()
     */
    show: function(req, res) {
        categoryModel.findOne({
            code: req.params.code
        }, function(err, category) {
            console.log(category);
            if (err) {
                return res.status(500).json({
                    message: 'Error getting category.'
                });
            }
            return res.json(category);
        });
    },
    /**
     * categoryController.create()
     */
    create: function(req, res) {
        req.checkBody('name', 'name is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            categoryModel.findOne({
                'name': req.body.name
            }, function(err, rows) {
                if (err) {
                    return res.status(404).json({
                        message: 'Error searching categories',
                        error: err
                    });
                }
                if (rows) {
                    return res.status(500).json({
                        message: 'The category already exists'
                    });
                } else {

                    var category = new categoryModel({
                        code: new Date().getTime(),
                        name: req.body.name,
                        description: req.body.description
                    });

                    category.save(function(err, category) {
                        if (err) {
                            return res.json(500, {
                                message: 'Error saving category',
                                error: err
                            });
                        }
                        return res.json({
                            message: 'saved',
                            _id: category._id
                        });
                    });
                }
            });
        } else {
            return res.status(500).json({
                title: 'Validation category',
                message: 'Failure to register due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * categoryController.update()
     */
    update: function(req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            categoryModel.findOne({
                code: code
            }, function(err, category) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting category',
                        error: err
                    });
                }
                if (!category) {
                    return res.status(404).json({
                        message: 'No such category'
                    });
                }

                var result = category;

                categoryModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function(err, category) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting category',
                            error: err
                        });
                    }

                    if (category) {
                        if (category.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.updated_at = new Date();

                            result.save(function(err, category) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving category.'
                                    });
                                }
                                if (!category) {
                                    return res.status(404).json({
                                        message: 'No such category'
                                    });
                                }
                                return res.json(category);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'Category already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.updated_at = new Date();

                        result.save(function(err, category) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving category.'
                                });
                            }
                            if (!category) {
                                return res.status(404).json({
                                    message: 'No such category'
                                });
                            }
                            return res.json(category);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update category',
                message: 'Failure to update category due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * categoryController.status()
     */
    status: function(req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            categoryModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function(err, category) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting category.'
                    });
                }

                if (!category) {
                    return res.status(404).json({
                        message: 'No such category'
                    });
                }

                return res.json(category);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status category',
                message: 'Failure to status category due to some validation error',
                errors: errors
            });
        }
    }
};
