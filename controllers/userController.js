var userModel = require('../models/userModel.js');
var md5 = require('crypto-js/md5');

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */

module.exports = {

    /**
     * userController.list()
     */
    list: function(req, res) {
        userModel.find(function(err, users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting users.'
                });
            }
            return res.json(users);
        });
    },
    /**
     * userController.listStatus()
     */
    listStatus: function(req, res) {
        var status = req.params.status;

        userModel.find({
            status: status
        }, function(err, users) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting users.'
                });
            }
            return res.json(users);
        });
    },
    /**
     * userController.list()
     */
    show: function(req, res) {
        userModel.findOne({
            code: req.params.code
        }, function(err, user) {
            console.log(user);
            if (err) {
                return res.status(500).json({
                    message: 'Error getting user.'
                });
            }
            return res.json(user);
        });
    },
    /**
     * userController.create()
     */
    create: function(req, res) {

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('username', 'username is required').notEmpty();
        req.checkBody('password', 'password is required').notEmpty();
        req.checkBody('role', 'role is required').notEmpty();


        var errors = req.validationErrors();
        if (!errors) {

            userModel.findOne({
                'username': req.body.username
            }, function(err, rows) {
                if (err) {
                    return res.status(404).json({
                        message: 'Error searching user',
                        error: err
                    });
                }
                if (rows) {
                    return res.status(500).json({
                        message: 'The username already exists'
                    });
                } else {

                    var user = new userModel({
                        code: new Date().getTime(),
                        name: req.body.name,
                        username: req.body.username,
                        password: md5(req.body.password),
                        role: req.body.role
                    });

                    user.save(function(err, user) {
                        if (err) {
                            return res.json(500, {
                                message: 'Error saving user',
                                error: err
                            });
                        }
                        return res.json({
                            message: 'saved',
                            _id: user._id
                        });
                    });
                }
            });
        } else {
            return res.status(500).json({
                title: 'Validation user',
                message: 'Failure to register due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * userController.update()
     */
    update: function(req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('username', 'username is required').notEmpty();
        req.checkBody('role', 'role is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            userModel.findOne({
                code: code
            }, function(err, user) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting user',
                        error: err
                    });
                }
                if (!user) {
                    return res.status(404).json({
                        message: 'No such user'
                    });
                }

                var result = user;

                userModel.findOne({
                    username: req.body.username.toLowerCase()
                }, function(err, user) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting user',
                            error: err
                        });
                    }

                    if (user) {
                        if (user.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.username = req.body.username ? req.body.username.toLowerCase() : result.username;
                            result.password = req.body.password ? md5(req.body.password) : result.password;
                            result.role = req.body.role ? req.body.role : result.role;
                            result.updated_at = new Date();

                            result.save(function(err, user) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving user.'
                                    });
                                }
                                if (!user) {
                                    return res.status(404).json({
                                        message: 'No such user'
                                    });
                                }
                                return res.json(user);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'username type already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.username = req.body.username ? req.body.username.toLowerCase() : result.username;
                        result.password = req.body.password ? md5(req.body.password) : result.password;
                        result.role = req.body.role ? req.body.role : result.role;
                        result.updated_at = new Date();

                        result.save(function(err, user) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving user.'
                                });
                            }
                            if (!identificationType) {
                                return res.status(404).json({
                                    message: 'No such user'
                                });
                            }
                            return res.json(user);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update user',
                message: 'Failure to update user due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * userController.status()
     */
    status: function(req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            userModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function(err, user) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting user.'
                    });
                }

                if (!user) {
                    return res.status(404).json({
                        message: 'No such user'
                    });
                }

                return res.json(user);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status user',
                message: 'Failure to status user due to some validation error',
                errors: errors
            });
        }
    }
};