var productModel = require('../models/productModel.js');
var ingredientModel = require('../models/ingredientModel.js');

/**
 * productController.js
 *
 * @description :: Server-side logic for managing products.
 */
module.exports = {

    /**
     * productController.list()
     */
    list: function(req, res) {
        productModel.find(function(err, products) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            return res.json(products);
        });
    },

    /**
     * productController.listStatus()
     */
    listStatus: function(req, res) {
        var status = req.params.status;

        productModel.find({
            status: status
        }, function(err, products) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            return res.json(products);
        });
    },

    /**
     * productController.show()
     */
    show: function(req, res) {
        var code = req.params.code;

        productModel.findOne({
            code: code
        }, function(err, product) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            if (!product) {
                return res.status(404).json({
                    message: 'No such product'
                });
            }
            return res.json(product);
        });
    },
    /**
     * productController.create()
     */
    create: function(req, res) {

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('price', 'price is required').notEmpty();
        req.checkBody('category', 'category is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            productModel.findOne({
                name: req.body.name.toLowerCase()
            }, function(err, product) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting product',
                        error: err
                    });
                }
                if (product) {
                    return res.status(500).json({
                        message: 'Product already exist'
                    });
                }
                var product = new productModel({
                    category: req.body.category,
                    code: new Date().getTime(),
                    name: req.body.name.toLowerCase(),
                    description: req.body.description ? req.body.description.toLowerCase() : "",
                    price: req.body.price
                });

                product.save(function(err, product) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error saving product',
                            error: err
                        });
                    }
                    return res.json({
                        message: 'saved',
                        _id: product._id
                    });
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation create product',
                message: 'Failure to create product due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * productController.update()
     */
    update: function(req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('price', 'price is required').notEmpty();
        req.checkBody('category', 'category is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            productModel.findOne({
                code: code
            }, function(err, product) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting product',
                        error: err
                    });
                }
                if (!product) {
                    return res.status(404).json({
                        message: 'No such product'
                    });
                }

                var result = product;

                productModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function(err, product) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting product',
                            error: err
                        });
                    }

                    if (product) {
                        if (product.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.category = req.body.category ? req.body.category : result.category;
                            result.updated_at = new Date();

                            result.save(function(err, product) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving product.'
                                    });
                                }
                                if (!product) {
                                    return res.status(404).json({
                                        message: 'No such product'
                                    });
                                }
                                return res.json(product);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'Product already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.category = req.body.category ? req.body.category : result.category;
                        result.updated_at = new Date();

                        result.save(function(err, product) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving product.'
                                });
                            }
                            if (!product) {
                                return res.status(404).json({
                                    message: 'No such product'
                                });
                            }
                            return res.json(product);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update product',
                message: 'Failure to update product due to some validation error',
                errors: errors
            });
        }
    },

    /**
     * productController.status()
     */
    status: function(req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            productModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function(err, product) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting product.'
                    });
                }

                if (!product) {
                    return res.status(404).json({
                        message: 'No such product'
                    });
                }

                return res.json(product);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status product',
                message: 'Failure to status product due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * productController.ingredients()
     */
    ingredients: function(req, res) {
        var code = req.params.code;

        productModel.findOne({
            code: code
        }, {
            code: 1,
            ingredients: 1
        }, function(err, product) {
            ingredientModel.populate(product, {
                path: "ingredients.ingredient"
            }, function(err, product) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting product.'
                    });
                }
                if (!product) {
                    return res.status(404).json({
                        message: 'No such product'
                    });
                }
                return res.json(product);
            });
        });
    },
    /**
     * productController.addIngredient()
     */
    addIngredient: function(req, res) {
        var code = req.params.code;

        productModel.findOneAndUpdate({
            code: code
        }, {
            $push: {
                ingredients: req.body.ingredient
            }
        }, function(err, product) {
            if (err) {
                return res.status(404).json({
                    message: 'Error searching product',
                    error: err
                });
            }
            if (!product) {
                return res.status(404).json({
                    message: 'No such product'
                });
            }
            return res.json(product);
        });
    },
    /**
     * productController.deleteIngredient()
     */
    deleteIngredient: function(req, res) {
        var code = req.params.code;

        productModel.findOneAndUpdate({
            $and: [{
                code: code
            }, {
                ingredients: {
                    $elemMatch: {
                        ingredient: req.body.ingredient
                    }
                }
            }]
        }, {
            $pull: {
                ingredients: {
                    ingredient: req.body.ingredient
                }
            }
        }, function(err, product) {
            if (err) {
                return res.status(404).json({
                    message: 'Error searching product',
                    error: err
                });
            }

            if (!product) {
                return res.status(404).json({
                    message: 'No such product'
                });
            }
            return res.json(product);
        });
    }

};