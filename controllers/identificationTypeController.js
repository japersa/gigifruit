var identificationTypeModel = require('../models/identificationTypeModel.js');

/**
 * identificationTypeController.js
 *
 * @description :: Server-side logic for managing identification  types
 */
module.exports = {

    /**
     * identificationTypeController.list()
     */
    list: function(req, res) {
        identificationTypeModel.find(function(err, identificationTypes) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting identification types.'
                });
            }
            return res.json(identificationTypes);
        });
    },
    /**
     * identificationTypeController.listStatus()
     */
    listStatus: function(req, res) {
        var status = req.params.status;

        identificationTypeModel.find({
            status: status
        }, function(err, identificationTypes) {
            if (err) {
                return res.status(500).json({
                    message: 'Error getting product.'
                });
            }
            return res.json(identificationTypes);
        });
    },
    /**
     * identificationTypeController.list()
     */
    show: function(req, res) {
        identificationTypeModel.findOne({
            code: req.params.code
        }, function(err, identificationType) {
            console.log(identificationType);
            if (err) {
                return res.status(500).json({
                    message: 'Error getting identification type.'
                });
            }
            return res.json(identificationType);
        });
    },
    /**
     * identificationTypeController.create()
     */
    create: function(req, res) {

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('abbreviation', 'abbreviation is required').notEmpty();


        var errors = req.validationErrors();
        if (!errors) {

            identificationTypeModel.findOne({
                'name': req.body.name
            }, function(err, rows) {
                if (err) {
                    return res.status(404).json({
                        message: 'Error searching identification type',
                        error: err
                    });
                }
                if (rows) {
                    return res.status(500).json({
                        message: 'The identification type already exists'
                    });
                } else {
            
                    var identificationType = new identificationTypeModel({
                        code: new Date().getTime(),
                        name:  req.body.name,
                        abbreviation: req.body.abbreviation,
                        description: req.body.description
                    });

                    identificationType.save(function(err, identificationType) {
                        if (err) {
                            return res.json(500, {
                                message: 'Error saving identification type',
                                error: err
                            });
                        }
                        return res.json({
                            message: 'saved',
                            _id: identificationType._id
                        });
                    });
                }
            });
        } else {
            return res.status(500).json({
                title: 'Validation identification type',
                message: 'Failure to register due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * identificationTypeController.update()
     */
    update: function(req, res) {
        var code = req.params.code;

        req.checkBody('name', 'name is required').notEmpty();
        req.checkBody('abbreviation', 'abbreviation is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {

            identificationTypeModel.findOne({
                code: code
            }, function(err, identificationType) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting identification type',
                        error: err
                    });
                }
                if (!identificationType) {
                    return res.status(404).json({
                        message: 'No such identification type'
                    });
                }

                var result = identificationType;

                identificationTypeModel.findOne({
                    name: req.body.name.toLowerCase()
                }, function(err, identificationType) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Error getting identification type',
                            error: err
                        });
                    }

                    if (identificationType) {
                        if (identificationType.code == code) {
                            result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                            result.abbreviation = req.body.abbreviation ? req.body.abbreviation.toLowerCase() : result.abbreviation;
                            result.description = req.body.description ? req.body.description.toLowerCase() : "";
                            result.updated_at = new Date();

                            result.save(function(err, identificationType) {
                                if (err) {
                                    return res.json(500, {
                                        message: 'Error saving identification type.'
                                    });
                                }
                                if (!identificationType) {
                                    return res.status(404).json({
                                        message: 'No such identification type'
                                    });
                                }
                                return res.json(identificationType);
                            });
                        } else {
                            return res.status(500).json({
                                message: 'Identification type already exist'
                            });
                        }
                    } else {
                        result.name = req.body.name ? req.body.name.toLowerCase() : result.name;
                        result.abbreviation = req.body.abbreviation ? req.body.abbreviation.toLowerCase() : result.abbreviation;
                        result.description = req.body.description ? req.body.description.toLowerCase() : "";
                        result.updated_at = new Date();

                        result.save(function(err, identificationType) {
                            if (err) {
                                return res.json(500, {
                                    message: 'Error saving identification type.'
                                });
                            }
                            if (!identificationType) {
                                return res.status(404).json({
                                    message: 'No such identification type'
                                });
                            }
                            return res.json(identificationType);
                        });
                    }
                });
            });
        } else {
            return res.status(500).json({
                title: 'Validation update identification type',
                message: 'Failure to update identification type due to some validation error',
                errors: errors
            });
        }
    },
    /**
     * identificationTypeController.status()
     */
    status: function(req, res) {
        var code = req.params.code;

        req.checkBody('status', 'status is required').notEmpty();

        var errors = req.validationErrors();
        if (!errors) {
            identificationTypeModel.findOneAndUpdate({
                code: code
            }, {
                status: req.body.status
            }, function(err, identificationType) {
                if (err) {
                    return res.json(500, {
                        message: 'Error getting identification type.'
                    });
                }

                if (!identificationType) {
                    return res.status(404).json({
                        message: 'No such identification type'
                    });
                }

                return res.json(identificationType);
            });
        } else {
            return res.status(500).json({
                title: 'Validation status identification type',
                message: 'Failure to status identification type due to some validation error',
                errors: errors
            });
        }
    }
};
