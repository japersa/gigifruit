var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var movementTypeSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
  "name": { type: String, require: true },
  "description": { type: String },
  "status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('movementType', movementTypeSchema);
