var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var unitModel = require('../models/unitModel.js');
var movementModel = require('../models/movementModel.js');

var ingredientSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
	"name" : { type: String, require: true },
  "description" : { type: String },
  "quantity": { type : Number , require: true },
  "unit" : { type: Schema.ObjectId, ref: 'unitModel' },
	"movements": [{ type: Schema.ObjectId, ref: 'movementModel' }],
	"status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('ingredient', ingredientSchema);
