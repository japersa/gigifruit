var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var userSchema = new Schema({	"code" : { type : String, require: true, unique: true },	"name" : { type: String, require: true },	"username" : { type: String, require: true, unique: true },	"password" : { type: String, require: true },	"role" : {type: String, require: true },	"status": { type: Number, default: 1 },  "created_at": { type: Date, default: Date.now },  "updated_at": { type: Date }});

module.exports = mongoose.model('user', userSchema);
