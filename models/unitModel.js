var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var unitSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
	"name" : { type: String, require: true },
  "description" : { type: String },
	"abbreviation": { type: String, require: true },
	"status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('unit', unitSchema);
