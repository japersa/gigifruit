var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var expenseSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
  "value": {type: Number, require: true },
  "description" : { type: String, require: true },
	"status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('expenseSchema', categorySchema);
