var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var customerModel = require('../models/customerModel.js');
var productModel = require('../models/productModel.js');

var orderSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
  "consecutive": { type: Number, require: true },
  "description": { type: String },
  "customer" : { type: Schema.ObjectId, ref: 'customerModel' },
  "products": [{ type: Schema.ObjectId, ref: 'productModel', "quantity": { type: Number }, "price": { type: Number } }],
  "status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('order', orderSchema);
