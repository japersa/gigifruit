var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var movementTypeModel = require('../models/movementTypeModel.js');

var movementSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
	"movementType": { type: Schema.ObjectId, ref: 'movementTypeModel' },
	"quantity": { type : Number , require: true },
  "status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('movement', movementSchema);
