var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var identificationTypeSchema = new Schema({
	"code" : { type: String, unique: true, require: true },
	"name" : { type: String, require: true },
	"abbreviation": { type: String, require: true },
	"description" : { type: String },
	"status": { type: Number, default: 1 },
	"created_at": { type: Date, default: Date.now },
	"updated_at": { type: Date }
});

module.exports = mongoose.model('identificationType', identificationTypeSchema);
