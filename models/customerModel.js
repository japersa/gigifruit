var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var identificationTypeModel = require('../models/identificationTypeModel.js');

var customerSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
	"name" : { type: String, require: true },
  "identification_type": { type: Schema.ObjectId, ref: 'identificationTypeModel' },
  "id_document" : { type: String, unique: true, require: true },
  "address": { type: String },
  "phone": { type: Number },
  "mobile_phone": { type: Number },
  "email": { type: String, require: true },
	"status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('customer', customerSchema);
