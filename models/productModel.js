var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var categoryModel = require('../models/categoryModel.js');
var ingredientModel = require('../models/ingredientModel.js');

var productSchema = new Schema({
	"code" : { type : String, require: true, unique: true },
	"name" : { type: String, require: true },
  "description" : { type: String },
  "price" : { type: Number, require: true },
  "category": { type: Schema.ObjectId, ref: 'categoryModel' },
  "ingredients": [{ "ingredient": { type: Schema.ObjectId, ref: 'ingredientModel' }, "quantity": { type : Number } }],
	"status": { type: Number, default: 1 },
  "created_at": { type: Date, default: Date.now },
  "updated_at": { type: Date }
});

module.exports = mongoose.model('product', productSchema);
