angular.module('generateOrder', [])

.controller('generateOrderCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

    $scope.customers = [];
    $scope.orderProducts = [];
    $scope.selectedCustomer = '';
    var index = 0;

    // dtOptions
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withBootstrap()
        .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
        .withColumnFilter({
            aoColumns: [{
                type: 'text',
                bRegex: true,
                bSmart: true
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }]
        });



    // dtColumnDefs
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2).notSortable()
    ];



    function getConsecutiveOrder() {
        api.getConsecutiveOrder().then(function(result) {
            console.log(result);
            $scope.currentDate = formatDate(result.data.currentDate);
            $scope.consecutive = result.data.consecutive;
        }, function(err) {
            console.log(err);
        });
    }


    function formatDate(date) {
        var d = new Date(date);
        var day = ("0" + d.getDate()).slice(-2);
        var month = ("0" + (d.getMonth() + 1)).slice(-2);
        var year = d.getFullYear();
        return day + "/" + month + "/" + year;
    }


    function getCustomers() {
        api.getCustomers().then(function(result) {
            console.log(result);
            $scope.customers = angular.copy(result.data);
        }, function(err) {

        });
    }


    function getProducts() {
        api.getProducts().then(function(result) {
            console.log(result);
            $scope.products = angular.copy(result.data);
        }, function(err) {

        });
    }

    $scope.openCustomersModal = function() {
        getCustomers();
        $('#customersModal').modal('show');
    }

    $scope.openProductsModal = function() {
        getProducts();
        $('#productsModal').modal('show');
    }

    $scope.resetCustomer = function() {
        $scope.selectedCustomer = "";
    }

    $scope.resetDetail = function() {
        $scope.selectedItem = "";
    }

    $scope.getSelectedCustomer = function(item) {
        $scope.selectedCustomer = angular.copy(item);
        $('#customersModal').modal('hide');
    }


    $scope.getSelectedProduct = function(item) {
        $scope.selectedItem = angular.copy(item);
        $('#quantity').focus();
        $('#productsModal').modal('hide');
    }


    $scope.addItemOrder = function(item) {
        if ($('#item_price').val() == '') {
            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                title: "<strong>Seleccione un producto</strong>",
                message: ""
            }, {
                type: 'danger'
            });
        } else if ($.trim($('#quantity').val()) == '') {
            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                title: "<strong>Seleccione un producto de la lista</strong>",
                message: ""
            }, {
                type: 'danger'
            });
            $('#quantity').focus();
        } else {
            index++;

            var amount = $scope.selectedItem.quantity * 1;
            var itemPrice = $scope.selectedItem.price * 1;


            $scope.orderProducts.push({
                code: $scope.selectedItem.code,
                name: $scope.selectedItem.name,
                price: $scope.selectedItem.price,
                quantity: $scope.selectedItem.quantity,
                total: ((itemPrice * amount)),
                id_index: index
            });
            //console.table($scope.billProducts);
            getValues();
            $scope.resetDetail();
        }

    }

    function getValues() {

        var total = 0;

        $scope.orderProducts.forEach(function(element, index, array) {
            total = (total * 1 + ($scope.orderProducts[index].total * 1));
        });

        $scope.total = total;
    }


    $scope.deleteItem = function(item_index) {

        for (var i = 0; i < $scope.orderProducts.length; i++) {

            if ($scope.orderProducts[i].id_index === item_index) {
                $scope.orderProducts.splice(i, 1);
                if ($scope.orderProducts.length == 0) {
                    index = 0;
                }
                getValues();
                //console.table($scope.orderProducts);
                break;
            }
        }
    }

    $scope.saveOrder = function() {

        if ($scope.selectedCustomer == '') {
            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                title: "<strong>Seleccione un cliente de la lista</strong>",
                message: ""
            }, {
                type: 'danger'
            });
            scrolltop();
            $scope.openCustomersModal();
        } else if ($scope.orderProducts.length == 0) {
            $.notify({
                icon: 'glyphicon glyphicon-warning-sign',
                title: "<strong>Seleccione un producto de la lista</strong>",
                message: ""
            }, {
                type: 'danger'
            });
            scrolltop();
            $scope.openProductsModal();
        } else {
            var response = confirm("¿Desea guardar el pedido?");
            if (response) {
                api.createOrder({
                    customer: $scope.selectedCustomer,
                    consecutive: $scope.consecutive,
                    products: $scope.orderProducts,
                    total: $scope.total
                }).then(function (result) {
                  
                }, function (err) {

                });
            }
        }
    }

    function scrolltop() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
    }


    getConsecutiveOrder();

});
