angular.module('ingredient', [])

.controller('ingredientCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

  $scope.ingredients = [];

  // dtOptions
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withBootstrap()
    .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
    .withColumnFilter({
      aoColumns: [{
        type: 'number'
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }]
    });


  // dtColumnDefs
  $scope.dtColumnDefs = [
    DTColumnDefBuilder.newColumnDef(0),
    DTColumnDefBuilder.newColumnDef(1),
    DTColumnDefBuilder.newColumnDef(2),
    DTColumnDefBuilder.newColumnDef(3).notSortable()
  ];


  $scope.openModal = function() {
    resetForm();
    getUnit();
    $('#ingredientModal').modal('show');
  }

  $scope.cancel = function() {
    resetForm();
    $('#ingredientModal').modal('hide');
  }

  function getIngredients() {
    api.getIngredients().then(function(result) {
      console.log(result);
      $scope.ingredients = angular.copy(result.data);
    }, function(err) {

    });
  }

  function getUnit() {
    api.getUnit().then(function(result) {
      console.log(result);
      $scope.units = angular.copy(result.data);
    }, function(err) {

    });
  }

  function showIngredient(code) {
    api.showIngredient(code).then(function(result) {
      console.log(result);
      $scope.ingredient = angular.copy(result.data);
    }, function(err) {

    });
  }

  $scope.editModal = function(code) {
    showIngredient(code);
    getUnit();
    $scope.editing = true;
    $('#ingredientModal').modal('show');
  }

  function resetForm() {
    $scope.editing = false;
    $scope.ingredient = "";
    $scope.ingredientForm.$setPristine();
  }

  $scope.save = function(ingredient) {
    api.createIngredient({
      name: ingredient.name,
      unit: ingredient.unit,
      quantity: ingredient.quantity,
      description: ingredient.description
    }).then(function(result) {
      console.log(result);
      getIngredients();
      resetForm();
      $('#ingredientModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Guardado exitoso</strong>",
        message: "Se ha guardado exitosamente el nuevo ingrediente"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.update = function(ingredient) {
    api.updateIngredient($scope.ingredient.code, {
      name: ingredient.name,
      unit: ingredient.unit,
      description: ingredient.description
    }).then(function(result) {
      console.log(result);
      getIngredients();
      resetForm();
      $('#ingredientModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Actualizado exitoso</strong>",
        message: "Se ha actualizado exitosamente el ingrediente"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.changeStatus = function(code, status) {
    api.changeIngredientStatus(code, {
      status: status
    }).then(function(result) {
      console.log(result);
      getIngredients();
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Cambio de estado exitoso</strong>",
        message: "Se ha actualizado exitosamente el ingrediente"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  //load
  getIngredients();

});
