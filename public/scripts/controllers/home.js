angular.module('main', [])

    .controller('mainCtrl', function ($scope, api) {

      api.showIndex().then(function (result) {
          $scope.index = result.data;
      },function (err) {
          console.log(err);
      });

    })
