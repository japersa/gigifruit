angular.module('user', [])

.controller('userCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

    $scope.roles = [{
        "name": "Administrador",
        "abbreviation": "admin"
    }, {
        "name": "Cajero",
        "abbreviation": "caja"
    }];


    $scope.users = [];

    // dtOptions
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withBootstrap()
        .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
        .withColumnFilter({
            aoColumns: [{
                type: 'number'
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }]
        });


    // dtColumnDefs
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];


    $scope.openModal = function() {
        resetForm();
        $('#userModal').modal('show');
    }

    $scope.cancel = function() {
        resetForm();
        $('#userModal').modal('hide');
    }

    function getUsers() {
        api.getUsers().then(function(result) {
            console.log(result);
            $scope.users = angular.copy(result.data);
        }, function(err) {

        });
    }

    function showUser(code) {
        api.showUser(code).then(function(result) {
            console.log(result);
            $scope.user = angular.copy(result.data);
            $scope.user.password = "";
        }, function(err) {

        });
    }

    $scope.editModal = function(code) {
        showUser(code);
        $scope.editing = true;
        $('#userModal').modal('show');
    }

    function resetForm() {
        $scope.editing = false;
        $scope.user = "";
        $scope.userForm.$setPristine();
    }

    $scope.save = function(user) {
        api.createUser({
            name: user.name,
            username: user.username,
            password: user.password,
            role: user.role
        }).then(function(result) {
            console.log(result);
            getUsers();
            resetForm();
            $('#userModal').modal('hide');
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Guardado exitoso</strong>",
                message: "Se ha guardado exitosamente el nuevo usuario"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    $scope.update = function(user) {
        api.updateUser($scope.user.code, {
            name: user.name,
            username: user.username,
            password: user.password,
            role: user.role
        }).then(function(result) {
            console.log(result);
            getUsers();
            resetForm();
            $('#userModal').modal('hide');
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Actualizado exitoso</strong>",
                message: "Se ha actualizado exitosamente el usuario"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    $scope.changeStatus = function(code, status) {
        api.changeUserStatus(code, {
            status: status
        }).then(function(result) {
            console.log(result);
            getUsers();
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Cambio de estado exitoso</strong>",
                message: "Se ha actualizado exitosamente el usuario"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    //load
    getUsers();

});