angular.module('unit', [])

.controller('unitCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

  $scope.units = [];

  // dtOptions
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withBootstrap()
    .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
    .withColumnFilter({
      aoColumns: [{
        type: 'number'
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }]
    });


  // dtColumnDefs
  $scope.dtColumnDefs = [
    DTColumnDefBuilder.newColumnDef(0),
    DTColumnDefBuilder.newColumnDef(1),
    DTColumnDefBuilder.newColumnDef(2),
    DTColumnDefBuilder.newColumnDef(3).notSortable()
  ];


  $scope.openModal = function() {
    resetForm();
    $('#unitModal').modal('show');
  }

  $scope.cancel = function() {
    resetForm();
    $('#unitModal').modal('hide');
  }

  function getUnit() {
    api.getUnit().then(function(result) {
      console.log(result);
      $scope.units = angular.copy(result.data);
    }, function(err) {

    });
  }

  function showUnit(code) {
    api.showUnit(code).then(function(result) {
      console.log(result);
      $scope.unit = angular.copy(result.data);
    }, function(err) {

    });
  }

  $scope.editModal = function(code) {
    showUnit(code);
    $scope.editing = true;
    $('#unitModal').modal('show');
  }

  function resetForm() {
    $scope.editing = false;
    $scope.unit = "";
    $scope.unitForm.$setPristine();
  }

  $scope.save = function(unit) {
    api.createUnit({
      name: unit.name,
      abbreviation: unit.abbreviation,
      description: unit.description
    }).then(function(result) {
      console.log(result);
      getUnit();
      resetForm();
      $('#unitModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Guardado exitoso</strong>",
        message: "Se ha guardado exitosamente la nueva unidad"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.update = function(unit) {
    api.updateUnit($scope.unit.code, {
      name: unit.name,
      abbreviation: unit.abbreviation,
      description: unit.description
    }).then(function(result) {
      console.log(result);
      getUnit();
      resetForm();
      $('#unitModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Actualizado exitoso</strong>",
        message: "Se ha actualizado exitosamente la unidad"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.changeStatus = function(code, status) {
    api.changeUnitStatus(code, {
      status: status
    }).then(function(result) {
      console.log(result);
      getUnit();
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Cambio de estado exitoso</strong>",
        message: "Se ha actualizado exitosamente la unidad"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  //load
  getUnit();

});
