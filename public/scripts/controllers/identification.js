angular.module('identification', [])

.controller('identificationCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

  $scope.identifications = [];

  // dtOptions
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withBootstrap()
    .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
    .withColumnFilter({
      aoColumns: [{
        type: 'number'
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }]
    });


  // dtColumnDefs
  $scope.dtColumnDefs = [
    DTColumnDefBuilder.newColumnDef(0),
    DTColumnDefBuilder.newColumnDef(1),
    DTColumnDefBuilder.newColumnDef(2),
    DTColumnDefBuilder.newColumnDef(3).notSortable()
  ];


  $scope.openModal = function() {
    resetForm();
    $('#identificationModal').modal('show');
  }

  $scope.cancel = function() {
    resetForm();
    $('#identificationModal').modal('hide');
  }

  function getIdentification() {
    api.getIdentification().then(function(result) {
      console.log(result);
      $scope.identifications = angular.copy(result.data);
    }, function(err) {

    });
  }

  function showIdentification(code) {
    api.showIdentification(code).then(function(result) {
      console.log(result);
      $scope.identification = angular.copy(result.data);
    }, function(err) {

    });
  }

  $scope.editModal = function(code) {
    showIdentification(code);
    $scope.editing = true;
    $('#identificationModal').modal('show');
  }

  function resetForm() {
    $scope.editing = false;
    $scope.identification = "";
    $scope.identificationForm.$setPristine();
  }

  $scope.save = function(identification) {
    api.createIdentification({
      name: identification.name,
      abbreviation: identification.abbreviation,
      description: identification.description
    }).then(function(result) {
      console.log(result);
      getIdentification();
      resetForm();
      $('#identificationModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Guardado exitoso</strong>",
        message: "Se ha guardado exitosamente el nuevo tipo de documento"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.update = function(identification) {
    api.updateIdentification($scope.identification.code, {
      name: identification.name,
      abbreviation: identification.abbreviation,
      description: identification.description
    }).then(function(result) {
      console.log(result);
      getIdentification();
      resetForm();
      $('#identificationModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Actualizado exitoso</strong>",
        message: "Se ha actualizado exitosamente el tipo de documento"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.changeStatus = function(code, status) {
    api.changeIdentificationStatus(code, {
      status: status
    }).then(function(result) {
      console.log(result);
      getIdentification();
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Cambio de estado exitoso</strong>",
        message: "Se ha actualizado exitosamente el tipo de documento"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  //load
  getIdentification();

});
