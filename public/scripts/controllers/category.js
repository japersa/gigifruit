angular.module('category', [])

.controller('categoryCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

  $scope.categories = [];

  // dtOptions
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withBootstrap()
    .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
    .withColumnFilter({
      aoColumns: [{
        type: 'number'
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }]
    });


  // dtColumnDefs
  $scope.dtColumnDefs = [
    DTColumnDefBuilder.newColumnDef(0),
    DTColumnDefBuilder.newColumnDef(1),
    DTColumnDefBuilder.newColumnDef(2),
    DTColumnDefBuilder.newColumnDef(3).notSortable()
  ];


  $scope.openModal = function() {
    resetForm();
    $('#categoryModal').modal('show');
  }

  $scope.cancel = function() {
    resetForm();
    $('#categoryModal').modal('hide');
  }

  function getCategories() {
    api.getCategories().then(function(result) {
      console.log(result);
      $scope.categories = angular.copy(result.data);
    }, function(err) {

    });
  }

  function showCategory(code) {
    api.showCategory(code).then(function(result) {
      console.log(result);
      $scope.category = angular.copy(result.data);
    }, function(err) {

    });
  }

  $scope.editModal = function(code) {
    showCategory(code);
    $scope.editing = true;
    $('#categoryModal').modal('show');
  }

  function resetForm() {
    $scope.editing = false;
    $scope.category = "";
    $scope.categoryForm.$setPristine();
  }

  $scope.save = function(category) {
    api.createCategory({
      name: category.name,
      description: category.description
    }).then(function(result) {
      console.log(result);
      getCategories();
      resetForm();
      $('#categoryModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Guardado exitoso</strong>",
        message: "Se ha guardado exitosamente la nueva categoria"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.update = function(category) {
    api.updateCategory($scope.category.code, {
      name: category.name,
      description: category.description
    }).then(function(result) {
      console.log(result);
      getCategories();
      resetForm();
      $('#categoryModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Actualizado exitoso</strong>",
        message: "Se ha actualizado exitosamente la categoria"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.changeStatus = function(code, status) {
    api.changeCategoryStatus(code, {
      status: status
    }).then(function(result) {
      console.log(result);
      getCategories();
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Cambio de estado exitoso</strong>",
        message: "Se ha actualizado exitosamente la categoria"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  //load
  getCategories();

});
