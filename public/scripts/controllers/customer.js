angular.module('customer', [])

.controller('customerCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

  $scope.customers = [];

  // dtOptions
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withBootstrap()
    .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
    .withColumnFilter({
      aoColumns: [{
        type: 'number'
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }, {
        type: 'text',
        bRegex: true,
        bSmart: true
      }]
    });


  // dtColumnDefs
  $scope.dtColumnDefs = [
    DTColumnDefBuilder.newColumnDef(0),
    DTColumnDefBuilder.newColumnDef(1),
    DTColumnDefBuilder.newColumnDef(2),
    DTColumnDefBuilder.newColumnDef(3).notSortable()
  ];


  $scope.openModal = function() {
    resetForm();
    getIdentification();
    $('#customerModal').modal('show');
  }

  $scope.cancel = function() {
    resetForm();
    $('#customerModal').modal('hide');
  }

  function getCustomers() {
    api.getCustomers().then(function(result) {
      console.log(result);
      $scope.customers = angular.copy(result.data);
    }, function(err) {

    });
  }

  function getIdentification() {
    api.getIdentification().then(function(result) {
      console.log(result);
      $scope.identifications = angular.copy(result.data);
    }, function(err) {

    });
  }

  function showCustomer(code) {
    api.showCustomer(code).then(function(result) {
      console.log(result);
      $scope.customer = angular.copy(result.data);
    }, function(err) {

    });
  }

  $scope.editModal = function(code) {
    showCustomer(code);
    getIdentification();
    $scope.editing = true;
    $('#customerModal').modal('show');
  }

  function resetForm() {
    $scope.editing = false;
    $scope.customer = "";
    $scope.customerForm.$setPristine();
  }

  $scope.save = function(customer) {
    api.createCustomer({
      name: customer.name,
      identification_type: customer.identification_type,
      id_document: customer.id_document,
      email: customer.email,
      address: customer.address,
      phone: customer.phone,
      mobile_phone: customer.mobile_phone
    }).then(function(result) {
      console.log(result);
      getCustomers();
      resetForm();
      $('#customerModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Guardado exitoso</strong>",
        message: "Se ha guardado exitosamente el nuevo cliente"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.update = function(customer) {
    api.updateCustomer($scope.customer.code, {
      name: customer.name,
      identification_type: customer.identification_type,
      id_document: customer.id_document,
      email: customer.email,
      address: customer.address,
      phone: customer.phone,
      mobile_phone: customer.mobile_phone
    }).then(function(result) {
      console.log(result);
      getCustomers();
      resetForm();
      $('#customerModal').modal('hide');
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Actualizado exitoso</strong>",
        message: "Se ha actualizado exitosamente el cliente"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  $scope.changeStatus = function(code, status) {
    api.changeCustomerStatus(code, {
      status: status
    }).then(function(result) {
      console.log(result);
      getCustomers();
      $.notify({
        icon: 'glyphicon glyphicon-ok',
        title: "<strong>Cambio de estado exitoso</strong>",
        message: "Se ha actualizado exitosamente el cliente"
      }, {
        type: 'success'
      });
    }, function(err) {
      console.log(err);
    });
  }

  //load
  getCustomers();

});
