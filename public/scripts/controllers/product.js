angular.module('product', [])

.controller('productCtrl', function($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder, api) {

    $scope.products = [];
    $scope.ingredients = [];
    $scope.productIngredients = [];

    // dtOptions
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withBootstrap()
        .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
        .withColumnFilter({
            aoColumns: [{
                type: 'number'
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }]
        });


    // dtColumnDefs
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];

    $scope.ingredientModal = function(code) {
        resetFormIngredients();
        productIngredients(code);
        getIngredients();
        $('#ingredientModal').modal('show');
    }


    $scope.openModal = function() {
        resetForm();
        getCategories();
        $('#productModal').modal('show');
    }

    $scope.cancel = function() {
        resetForm();
        $('#productModal').modal('hide');
    }

    function getProducts() {
        api.getProducts().then(function(result) {
            console.log(result);
            $scope.products = angular.copy(result.data);
        }, function(err) {

        });
    }

    function getCategories() {
        api.getCategories().then(function(result) {
            console.log(result);
            $scope.categories = angular.copy(result.data);
        }, function(err) {

        });
    }

    function showProduct(code) {
        api.showProduct(code).then(function(result) {
            console.log(result);
            $scope.product = angular.copy(result.data);
        }, function(err) {

        });
    }

    function productIngredients(code) {
        api.productIngredients(code).then(function(result) {
            console.log(result);
            $scope.productIngredients = angular.copy(result.data);
        }, function(err) {

        });
    }

    function getIngredients() {
        api.getIngredients().then(function(result) {
            console.log(result);
            $scope.ingredients = angular.copy(result.data);
        }, function(err) {

        });
    }


    $scope.addProductIngredient = function(productIngredient) {
        var ingredient = $scope.productIngredients.ingredients.map(function(column) {
            return column.ingredient._id
        }).indexOf(productIngredient.ingredient);

        if ($scope.productIngredients.ingredients[ingredient] == undefined) {
            api.addProductIngredient($scope.productIngredients.code, {
                ingredient: {
                    ingredient: productIngredient.ingredient,
                    quantity: productIngredient.quantity
                }
            }).then(function(result) {
                console.log(result);
                productIngredients($scope.productIngredients.code);
                resetFormIngredients();
                $.notify({
                    icon: 'glyphicon glyphicon-ok',
                    title: "<strong>Actualizado exitoso</strong>",
                    message: "Se ha actualizado exitosamente el producto"
                }, {
                    type: 'success'
                });
            }, function(err) {
                console.log(err);
            });
        } else {
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Este ingrediente ya se encuentra agregado</strong>",
                message: ""
            }, {
                type: 'danger'
            });
        }
    }

    $scope.deleteProductIngredient = function(productIngredient) {
        console.log(productIngredient);
        api.deleteProductIngredient($scope.productIngredients.code, {
            ingredient: productIngredient.ingredient._id
        }).then(function(result) {
            console.log(result);
            productIngredients($scope.productIngredients.code);

            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Eliminado exitoso</strong>",
                message: "Se ha eliminado exitosamente el ingrediente"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    $scope.editModal = function(code) {
        showProduct(code);
        getCategories();
        $scope.editing = true;
        $('#productModal').modal('show');
    }

    function resetForm() {
        $scope.editing = false;
        $scope.product = "";
        $scope.productForm.$setPristine();
    }

    function resetFormIngredients() {
        $scope.productIngredient = "";
        $scope.productIngredientForm.$setPristine();
    }

    $scope.save = function(product) {
        api.createProduct({
            name: product.name,
            category: product.category,
            price: product.price,
            description: product.description
        }).then(function(result) {
            console.log(result);
            getProducts();
            resetForm();
            $('#productModal').modal('hide');
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Guardado exitoso</strong>",
                message: "Se ha guardado exitosamente el nuevo producto"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    $scope.update = function(product) {
        api.updateProduct($scope.product.code, {
            name: product.name,
            category: product.category,
            price: product.price,
            description: product.description
        }).then(function(result) {
            console.log(result);
            getProducts();
            resetForm();
            $('#productModal').modal('hide');
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Actualizado exitoso</strong>",
                message: "Se ha actualizado exitosamente el producto"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    $scope.changeStatus = function(code, status) {
        api.changeProductStatus(code, {
            status: status
        }).then(function(result) {
            console.log(result);
            getProducts();
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                title: "<strong>Cambio de estado exitoso</strong>",
                message: "Se ha actualizado exitosamente el producto"
            }, {
                type: 'success'
            });
        }, function(err) {
            console.log(err);
        });
    }

    //load
    getProducts();

});