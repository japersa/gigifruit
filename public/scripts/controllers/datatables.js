angular.module('datatablestest', [])

.controller('datatableCtrl', function ($scope, $state, $http, DTOptionsBuilder, DTColumnDefBuilder) {


    $scope.products = [{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    },{
      id: "1",
      nombre: "Prueba",
      estado: "Activo"
    }];

    // dtOptions
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withBootstrap()
        .withLanguageSource('https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json')
        .withColumnFilter({
            aoColumns: [{
                type: 'number'
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }, {
                type: 'text',
                bRegex: true,
                bSmart: true
            }]
        });


    // dtColumnDefs
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];


    $scope.openModal = function () {
        $('#categoryModal').modal('show');
    }

});
