angular.module('services', [])

.factory('api', ['$q', '$http', function($q, $http) {

    // Url path for production server
    var server = '/';


    function getIdentification() {
        /* Get identification type  */
        var defered = $q.defer();
        $http.get(server + 'identification/types').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showIdentification(code) {
        /* Show identification type  */
        var defered = $q.defer();
        $http.get(server + 'identification/types/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createIdentification(object) {
        /* Create identification type  */
        var defered = $q.defer();
        $http.post(server + 'identification/types', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateIdentification(code, object) {
        /* update identification type */
        var defered = $q.defer();
        $http.put(server + 'identification/types/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeIdentificationStatus(code, status) {
        /* change status for identification */
        var defered = $q.defer();
        $http.put(server + 'identification/types/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function getUnit() {
        /* Get unit  */
        var defered = $q.defer();
        $http.get(server + 'units').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showUnit(code) {
        /* Show unit  */
        var defered = $q.defer();
        $http.get(server + 'units/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createUnit(object) {
        /* Create unit  */
        var defered = $q.defer();
        $http.post(server + 'units', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateUnit(code, object) {
        /* update unit */
        var defered = $q.defer();
        $http.put(server + 'units/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeUnitStatus(code, status) {
        /* change status for unit */
        var defered = $q.defer();
        $http.put(server + 'units/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function getCustomers() {
        /* Get customers  */
        var defered = $q.defer();
        $http.get(server + 'customers').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showCustomer(code) {
        /* Show customer  */
        var defered = $q.defer();
        $http.get(server + 'customers/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createCustomer(object) {
        /* Create customer  */
        var defered = $q.defer();
        $http.post(server + 'customers', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateCustomer(code, object) {
        /* update customer */
        var defered = $q.defer();
        $http.put(server + 'customers/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeCustomerStatus(code, status) {
        /* change status for customer */
        var defered = $q.defer();
        $http.put(server + 'customers/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }



    function getCategories() {
        /* Get categories  */
        var defered = $q.defer();
        $http.get(server + 'categories').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showCategory(code) {
        /* Show category  */
        var defered = $q.defer();
        $http.get(server + 'categories/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createCategory(object) {
        /* Create category  */
        var defered = $q.defer();
        $http.post(server + 'categories', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateCategory(code, object) {
        /* update category */
        var defered = $q.defer();
        $http.put(server + 'categories/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeCategoryStatus(code, status) {
        /* change status for category */
        var defered = $q.defer();
        $http.put(server + 'categories/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function getProducts() {
        /* Get products  */
        var defered = $q.defer();
        $http.get(server + 'products').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showProduct(code) {
        /* Show product  */
        var defered = $q.defer();
        $http.get(server + 'products/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createProduct(object) {
        /* Create product  */
        var defered = $q.defer();
        $http.post(server + 'products', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateProduct(code, object) {
        /* update product */
        var defered = $q.defer();
        $http.put(server + 'products/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeProductStatus(code, status) {
        /* change status for product */
        var defered = $q.defer();
        $http.put(server + 'products/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function getIngredients() {
        /* Get ingredients  */
        var defered = $q.defer();
        $http.get(server + 'ingredients').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showIngredient(code) {
        /* Show ingredient  */
        var defered = $q.defer();
        $http.get(server + 'ingredients/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createIngredient(object) {
        /* Create ingredient  */
        var defered = $q.defer();
        $http.post(server + 'ingredients', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateIngredient(code, object) {
        /* update ingredient */
        var defered = $q.defer();
        $http.put(server + 'ingredients/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeIngredientStatus(code, status) {
        /* change status for ingredient */
        var defered = $q.defer();
        $http.put(server + 'ingredients/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showIndex() {
        /* Show index  */
        var defered = $q.defer();
        $http.get(server + 'show').then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function getUsers() {
        /* Get users  */
        var defered = $q.defer();
        $http.get(server + 'users').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function showUser(code) {
        /* Show user  */
        var defered = $q.defer();
        $http.get(server + 'users/show/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createUser(object) {
        /* Create user  */
        var defered = $q.defer();
        $http.post(server + 'users', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function updateUser(code, object) {
        /* update user */
        var defered = $q.defer();
        $http.put(server + 'users/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function changeUserStatus(code, status) {
        /* change status for user */
        var defered = $q.defer();
        $http.put(server + 'users/status/' + code, status).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    function getOrders() {
        /* Get orders  */
        var defered = $q.defer();
        $http.get(server + 'orders').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function getConsecutiveOrder() {
        /* Get consecutive order  */
        var defered = $q.defer();
        $http.get(server + 'orders/code').then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function productIngredients(code) {
        /* Show product ingredients  */
        var defered = $q.defer();
        $http.get(server + 'products/ingredients/' + code).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function addProductIngredient(code, object) {
        /* add product ingredient */
        var defered = $q.defer();
        $http.put(server + 'products/ingredient/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function deleteProductIngredient(code, object) {
        /* delete product ingredient */
        var defered = $q.defer();
        $http.put(server + 'products/ingredient/delete/' + code, object).then(defered.resolve, defered.reject);
        return defered.promise;
    }

    function createOrder(object) {
        /* create order */
        var defered = $q.defer();
        $http.post(server + 'orders/', object).then(defered.resolve, defered.reject);
        return defered.promise;
    }


    return {
        createIdentification: createIdentification,
        getIdentification: getIdentification,
        showIdentification: showIdentification,
        updateIdentification: updateIdentification,
        changeIdentificationStatus: changeIdentificationStatus,
        getUnit: getUnit,
        showUnit: showUnit,
        createUnit: createUnit,
        updateUnit: updateUnit,
        changeUnitStatus: changeUnitStatus,
        getCustomers: getCustomers,
        showCustomer: showCustomer,
        createCustomer: createCustomer,
        updateCustomer: updateCustomer,
        changeCustomerStatus: changeCustomerStatus,
        getCategories: getCategories,
        showCategory: showCategory,
        createCategory: createCategory,
        updateCategory: updateCategory,
        changeCategoryStatus: changeCategoryStatus,
        getProducts: getProducts,
        showProduct: showProduct,
        createProduct: createProduct,
        updateProduct: updateProduct,
        changeProductStatus: changeProductStatus,
        getIngredients: getIngredients,
        showIngredient: showIngredient,
        createIngredient: createIngredient,
        updateIngredient: updateIngredient,
        changeIngredientStatus: changeIngredientStatus,
        showIndex: showIndex,
        getUsers: getUsers,
        showUser: showUser,
        createUser: createUser,
        updateUser: updateUser,
        changeUserStatus: changeUserStatus,
        getOrders: getOrders,
        getConsecutiveOrder: getConsecutiveOrder,
        productIngredients: productIngredients,
        addProductIngredient: addProductIngredient,
        deleteProductIngredient: deleteProductIngredient,
        createOrder: createOrder
    }

}]);
