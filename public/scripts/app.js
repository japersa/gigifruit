angular.module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'CustomFilter',
    'datatables',
    'datatables.bootstrap',
    'datatables.columnfilter',
    'angular-loading-bar',
    'datatablestest',
    'category',
    'unit',
    'customer',
    'product',
    'ingredient',
    'user',
    'identification',
    'services',
    'main',
    'generateOrder'
])


.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
    });

    $urlRouterProvider.otherwise('/dashboard/home');

    $stateProvider
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'views/dashboard/main.html',
            resolve: {
                loadMyDirectives: function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                            name: 'sbAdminApp',
                            files: [
                                'scripts/directives/header/header.js',
                                'scripts/directives/header/header-notification/header-notification.js',
                                'scripts/directives/sidebar/sidebar.js',
                                'scripts/directives/sidebar/sidebar-search/sidebar-search.js'
                            ]
                        }),
                        $ocLazyLoad.load({
                            name: 'toggle-switch',
                            files: ["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                            ]
                        }),
                        $ocLazyLoad.load({
                            name: 'ngAnimate',
                            files: ['bower_components/angular-animate/angular-animate.js']
                        })
                    $ocLazyLoad.load({
                        name: 'ngCookies',
                        files: ['bower_components/angular-cookies/angular-cookies.js']
                    })
                    $ocLazyLoad.load({
                        name: 'ngResource',
                        files: ['bower_components/angular-resource/angular-resource.js']
                    })
                    $ocLazyLoad.load({
                        name: 'ngSanitize',
                        files: ['bower_components/angular-sanitize/angular-sanitize.js']
                    })
                    $ocLazyLoad.load({
                        name: 'ngTouch',
                        files: ['bower_components/angular-touch/angular-touch.js']
                    })
                }
            }
        })
        .state('dashboard.home', {
            url: '/home',
            templateUrl: 'views/dashboard/home.html',
            controller: 'mainCtrl',
            resolve: {
                loadMyFiles: function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'sbAdminApp',
                        files: [
                            'scripts/directives/timeline/timeline.js',
                            'scripts/directives/notifications/notifications.js',
                            'scripts/directives/chat/chat.js',
                            'scripts/directives/dashboard/stats/stats.js'
                        ]
                    })
                }
            }
        })

    .state('dashboard.customers', {
        templateUrl: 'views/templates/customers.html',
        url: '/customers',
        controller: 'customerCtrl'
    })

    .state('dashboard.category', {
        templateUrl: 'views/templates/category.html',
        url: '/category',
        controller: 'categoryCtrl'
    })

    .state('dashboard.identification', {
        templateUrl: 'views/templates/identification.html',
        url: '/identification',
        controller: 'identificationCtrl'
    })

    .state('dashboard.ingredients', {
        templateUrl: 'views/templates/ingredients.html',
        url: '/ingredients',
        controller: 'ingredientCtrl'
    })

    .state('dashboard.order', {
        templateUrl: 'views/templates/generateOrder.html',
        url: '/order',
        controller: 'generateOrderCtrl'
    })

    .state('dashboard.products', {
        templateUrl: 'views/templates/products.html',
        url: '/products',
        controller: 'productCtrl'
    })

    .state('dashboard.units', {
        templateUrl: 'views/templates/units.html',
        url: '/units',
        controller: 'unitCtrl'
    })

    .state('dashboard.users', {
        templateUrl: 'views/templates/users.html',
        url: '/users',
        controller: 'userCtrl'
    })

}]);
