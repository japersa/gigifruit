var express = require('express');
var router = express.Router();
var path = require('path');
var indexController = require('../controllers/indexController.js');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/index.html'));
});


/*
* GET
*/
router.get('/show', function(req, res) {
  indexController.show(req, res);
});

module.exports = router;
