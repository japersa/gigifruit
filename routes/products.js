var express = require('express');
var router = express.Router();
var productController = require('../controllers/productController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    productController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function(req, res) {
    productController.listStatus(req, res);
});


/*
 * GET
 */
router.get('/show/:code', function(req, res) {
    productController.show(req, res);
});

/*
 * GET
 */
router.get('/ingredients/:code', function(req, res) {
    productController.ingredients(req, res);
});



/*
 * POST
 */
router.post('/', function(req, res) {
    productController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function(req, res) {
    productController.update(req, res);
});

/*
 * PUT
 */
router.put('/status/:code', function(req, res) {
    productController.status(req, res);
});

/*
 * PUT
 */
router.put('/ingredient/:code', function(req, res) {
    productController.addIngredient(req, res);
});

/*
 * PUT
 */
router.put('/ingredient/delete/:code', function(req, res) {
    productController.deleteIngredient(req, res);
});



module.exports = router;