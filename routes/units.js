var express = require('express');
var router = express.Router();
var unitController = require('../controllers/unitController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    unitController.list(req, res);
});

/*
 * GET
 */
router.get('/show/:code', function(req, res) {
    unitController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    unitController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function(req, res) {
    unitController.update(req, res);
});


/*
 * PUT
 */
router.put('/status/:code', function(req, res) {
    unitController.status(req, res);
});


module.exports = router;
