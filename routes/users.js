var express = require('express');
var router = express.Router();
var userController = require('../controllers/userController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    userController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function(req, res) {
    userController.listStatus(req, res);
});


/*
 * GET
 */
router.get('/show/:code', function(req, res) {
    userController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    userController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function(req, res) {
    userController.update(req, res);
});

/*
 * PUT
 */
router.put('/status/:code', function(req, res) {
    userController.status(req, res);
});

module.exports = router;