var express = require('express');
var router = express.Router();
var identificationTypeController = require('../controllers/identificationTypeController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    identificationTypeController.list(req, res);
});

/*
 * GET
 */
router.get('/show/:code', function(req, res) {
    identificationTypeController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    identificationTypeController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function(req, res) {
    identificationTypeController.update(req, res);
});


/*
 * PUT
 */
router.put('/status/:code', function(req, res) {
    identificationTypeController.status(req, res);
});


module.exports = router;
