var express = require('express');
var router = express.Router();
var customerController = require('../controllers/customerController.js');

/*
 * GET
 */
router.get('/', function (req, res) {
    customerController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function (req, res) {
    customerController.listStatus(req, res);
});

/*
 * GET
 */
router.get('/show/:code', function (req, res) {
    customerController.show(req, res);
});

/*
 * POST
 */
router.post('/', function (req, res) {
    customerController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function (req, res) {
    customerController.update(req, res);
});

/*
 * PUT
 */
router.put('/status/:code', function (req, res) {
    customerController.status(req, res);
});


module.exports = router;
