var express = require('express');
var router = express.Router();
var categoryController = require('../controllers/categoryController.js');

/*
 * GET
 */
router.get('/', function (req, res) {
    categoryController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function (req, res) {
    categoryController.listStatus(req, res);
});


/*
 * GET
 */
router.get('/show/:code', function (req, res) {
    categoryController.show(req, res);
});

/*
 * POST
 */
router.post('/', function (req, res) {
    categoryController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function (req, res) {
    categoryController.update(req, res);
});

/*
 * PUT
 */
router.put('/status/:code', function (req, res) {
    categoryController.status(req, res);
});

module.exports = router;
