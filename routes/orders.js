var express = require('express');
var router = express.Router();
var orderController = require('../controllers/orderController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    orderController.list(req, res);
});

/*
 * GET
 */
router.get('/code', function(req, res) {
    orderController.generateConsecutive(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    orderController.create(req, res);
});


module.exports = router;
