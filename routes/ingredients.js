var express = require('express');
var router = express.Router();
var ingredientController = require('../controllers/ingredientController.js');

/*
 * GET
 */
router.get('/', function (req, res) {
    ingredientController.list(req, res);
});

/*
 * GET
 */
router.get('/status/:status', function (req, res) {
    ingredientController.listStatus(req, res);
});


/*
 * GET
 */
router.get('/show/:code', function (req, res) {
    ingredientController.show(req, res);
});

/*
 * POST
 */
router.post('/', function (req, res) {
    ingredientController.create(req, res);
});

/*
 * PUT
 */
router.put('/:code', function (req, res) {
    ingredientController.update(req, res);
});

/*
 * PUT
 */
router.put('/status/:code', function (req, res) {
    ingredientController.status(req, res);
});

module.exports = router;
